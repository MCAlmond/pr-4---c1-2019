
from flask import Flask, redirect, url_for, render_template, request, session, flash


app = Flask(__name__)

@app.route('/')
def iniciar():
    if 'nombre' in session:
        return redirect(url_for('arrancar, arrancar2, arrancar3'))
    return render_template('index.html')


@app.route('/Gracias/')
def gracias():
    return render_template('Gracias.html')

@app.route('/resultados/')
def resultados():
    return render_template('resultados.html')

@app.route('/index', methods=['POST', 'GET'])
def arrancar2():
    if request.method == 'GET':
        return redirect(url_for('iniciar'))
    elif request.method == 'POST':
        return render_template('index.html')
    else:
        return 'Metodo no valido'


@app.route('/Gracias', methods=['POST', 'GET'])
def arrancar():
    if request.method == 'GET':
        return redirect(url_for('iniciar'))
    elif request.method == 'POST':
        return render_template('Gracias.html')
    else:
        return 'Metodo no valido'

@app.route('/resultados', methods=['POST', 'GET'])
def arrancar3():
    if request.method == 'GET':
        return redirect(url_for('iniciar'))
    elif request.method == 'POST':
        return render_template('resultados.html')
    else:
        return 'Metodo no valido'

if __name__ == '__main__':
    app.run()
    # ruta = 'C:\\Users\\Asus\\PycharmProjects\\Taller 1 Voto\\votos registrados.txt'
    # file = open(ruta2, 'a+')
    # file.write('transaccion = % s' % transaccion + '\n')
    # file.close()